import {
  HttpClient
} from '@angular/common/http';
import {
  Injectable
} from '@angular/core';
import {
  Observable
} from 'rxjs';
import {
  environment
} from 'src/environments/environment';
import {
  tap
} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpClient: HttpClient
  ) {}

  isAuth(): boolean {
    return !!this.token;
  }

  private setToken(response: any | null) {
    if (response) {
      const res = JSON.parse(response);
      localStorage.setItem('accessToken', res.accessToken);
    } else {
      localStorage.clear();
    }
  }

  get token() {
    return localStorage.getItem('accessToken');
  }

  login(user: {
    username: string,
    password: string
  }): Observable < any > {
    return this.httpClient.post(
        `${environment.host}/login`, user, {
          responseType: 'text'
        }
      )
      .pipe(
        tap(this.setToken)
      )
  }

  logout() {
    this.setToken(null);
  }
}
