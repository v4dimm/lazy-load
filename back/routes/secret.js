var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');
const process = require('process');

const accessTokenSecret = process.env.ACCESSTOKENSECRET;

const flag = process.env.FLAG;

const authJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;
    console.log(accessTokenSecret);

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }

            console.log(user);
            req.user = user;
            next();
        })
    } else {
        res.sendStatus(401);
    }
};

/* GET flag. */
router.get('/', authJWT, (req, res, next) => {

    const {
        role
    } = req.user;

    if (role !== 'admin') {
        return res.sendStatus(403);
    }

    res.send(flag);
});

module.exports = router;