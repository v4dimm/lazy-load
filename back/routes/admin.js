var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');
const process = require('process');

const accessTokenSecret = process.env.ACCESSTOKENSECRET;

const authJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }

            console.log(user);
            req.user = user;
            next();
        })
    } else {
        res.sendStatus(401);
    }
};

/* GET admin role. */
router.get('/get', authJWT, (req, res, next) => {
    const {
        username,
        role
    } = req.user;

    if (role !== 'admin') {
        const accessToken = jwt.sign({
                username: username,
                role: 'admin'
            },
            accessTokenSecret, {
                expiresIn: '20m'
            }
        );

        res.json({
            accessToken,
        });
    }
});

module.exports = router;