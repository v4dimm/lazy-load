var express = require('express');
var router = express.Router();

const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const process = require('process');

const parseForm = bodyParser.json();

const accessTokenSecret = process.env.ACCESSTOKENSECRET;


const users = [{
        username: 'admin',
        password: '3qNkRpD5zy8cB223',
        role: 'admin'
    },
    {
        username: 'user',
        password: 'user',
        role: 'member'
    }
];


router.post('/', parseForm, (req, res) => {
    const {
        username,
        password
    } = req.body;

    const user = users.find(u => {
        return u.username === username && u.password === password
    });

    if (user) {
        const accessToken = jwt.sign({
                username: user.username,
                role: user.role
            },
            accessTokenSecret, {
                expiresIn: '20m'
            }
        );

        res.json({
            accessToken,
        });
    } else {
        res.status(401).send('Username or password incorrect');
    }
});

// router.post('/logout', (req, res) => {
//     const {
//         token
//     } = req.body;
//     refreshTokens = refreshTokens.filter(token => t !== token);
//     res.send('Logout successful');
// })

module.exports = router;